const jQuery = require('jquery');
global.Tether = require('tether');
const bootstrap = require('bootstrap');
const $ = jQuery;
const {
    _
} = require('./js/functions.js')
const remote = require('electron').remote
const {
    dialog
} = remote
const fs = require('fs')
const {
    execute,
    stopExec,
    yo
} = remote.require('./index.js')
const langs = [
    'Javascript',
    'HTML',
    'Python',
    'PHP',
    'Ruby'
]

let editor, session,
    fontsize = 15,
    currentFile = null,
    lang = 'javascript',
    paths = {
        python: '/usr/bin/env python',
        php: '/usr/bin/env php',
        ruby: '/usr/bin/env ruby',
    }

$(document).ready(() => {

    editor = ace.edit('editor')
    editor.setTheme("ace/theme/xcode")
    session = editor.getSession()
    session.setMode("ace/mode/javascript")
    session.setUseWrapMode(true)
    session.setTabSize(4)
    editor.insert("//Code away!")
    editor.focus()

    //Populate the lang dropdown dynamically
    let elem
    langs.forEach(item => {
        elem = `${elem === undefined ? '' : elem}<a class="dropdown-item" href="#">${item}</a>`
    })
    _('langdropdown').innerHTML = elem

    $(".dropdown-item").click(function() {
        $('#lang').html($(this).text())
    })

    $("body").on('DOMSubtreeModified', "span[id='lang']", function() {
        lang = $(this).html().toLowerCase()
        session.setMode(`ace/mode/${lang}`)

        editor.focus()
    })


    $('#new').on('click', () => {
        if (currentFile !== null) {
            swal({
                title: 'Do you want to close the current file?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(result => {
                currentFile = null
                editor.setValue("")
            }).catch(result => {
                return
            })
        } else {
            editor.setValue("")
        }
    })

    $('#zoom_in').on('click', () => {
        _('editor').style.fontSize = ++fontsize + 'px'
    })
    $('#zoom_out').on('click', () => {
        _('editor').style.fontSize = --fontsize + 'px'
    })
    $('#run').on('click', () => {
        execute(editor.getValue(), lang, paths)
    })
    $('#stop').on('click', () => {
        stopExec()
    })
    $('#save').on('click', () => {
        let saveFile = fileName => {
            fs.writeFile(fileName, editor.getValue(), err => {
                if (err) {
                    swal('Error!', 'An error occurred when creating the file ' + err.message, 'error')
                } else {
                    currentFile = fileName
                    swal('Done!', 'The file has been saved successfully', 'success')
                }
            })
        }
        if (currentFile === null) {
            dialog.showSaveDialog(fileName => {
                if (fileName === undefined) {
                    swal('Oops!', 'You did not save the file', 'warning')
                    return
                }
                saveFile(fileName)
            })
        } else {
            saveFile(currentFile)
        }
    })

    $('#open').on('click', () => {
        let openFile = fileName => {
            fs.readFile(fileName, 'utf-8', (err, data) => {
                if (err) {
                    swal('Error!', 'An error occurred when opening the file ' + err.message, 'error')
                } else {
                    currentFile = fileName
                    editor.setValue(data)
                }
            })
        }
        dialog.showOpenDialog({
            filters: [{
                name: 'All files',
                extensions: ['*']
            }],
            properties: ['openFile']
        }, fileNames => {
            if (fileNames === undefined) {
                swal('Oops!', 'No file selected', 'warning')
            } else {
                openFile(fileNames[0])
            }
        })
    })

    $('#setpath').on('click', () => {
        //         <div class="input-group">
        //   <span class="input-group-addon" id="basic-addon1">@</span>
        //   <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        // </div>
        swal({
            title: '<h4><strong>Settings</strong></h4>',
            // type: 'info',
            html: `<br><div class="input-group">
                <span class="input-group-addon">PHP</span>
                <input type="text" class="form-control" placeholder="Enter path here..." id="php_path">
                </div><br>
                <div class="input-group">
                <span class="input-group-addon">Python</span>
                <input type="text" class="form-control" placeholder="Enter path here..." id="python_path">
                </div><br>
                <div class="input-group">
                <span class="input-group-addon">Ruby</span>
                <input type="text" class="form-control" placeholder="Enter path here..." id="ruby_path">
                </div>`,
            showCloseButton: true,
            confirmButtonColor: 'blue',
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Save'
        }).then(isConfirm => {
            if (isConfirm) {
                setPath('php', $('#php_path').val())
                setPath('python', $('#python_path').val())
                setPath('ruby', $('#ruby_path').val())
            }
            editor.focus()
        }).catch(e => {
            editor.focus()
        })
    })

    function setPath(lang, path) {
        switch (lang) {
            case 'python':
                paths.python = path.trim() == '' ? '/usr/bin/env python' : path
                break
            case 'php':
                paths.php = path.trim() == '' ? '/usr/bin/env php' : path
                break
            case 'ruby':
                paths.ruby = path.trim() == '' ? '/usr/bin/env ruby' : path
                break
        }
    }
})
