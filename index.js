const electron = require('electron')
const {
    app,
    BrowserWindow,
    Menu,
    dialog
} = electron
const fs = require('fs')
const python = require('python-shell')
const runner = require("child_process");
const tmp = require('tmp')
const path = require('path')
const Positioner = require('electron-positioner')
let out = null,
    tempDir, url

try {
    tempDir = app.getPath('temp')
} catch (e) {
    tempDir = app.getPath('appData')
}

let subTmpDir = tmp.dirSync({
    mode: 0777,
    template: `${tempDir}${path.sep}tmp-XXXXXXXX`,
    unsafeCleanup: true
})

tempDir = subTmpDir.name

app.on('ready', () => {
    let win = new BrowserWindow({
        center: true,
        title: 'Codeaway'
    })
    win.setMenu(null)
    win.loadURL(`file://${__dirname}/main.html`)
    // win.openDevTools()
})

let newWin = () => {
    let win = new BrowserWindow({
        title: 'Codeaway - output',
    })
    let positioner = new Positioner(win)
    positioner.move('topRight')
    return win
}

exports.execute = (content, language, paths) => {

    let makeHTML = (err, out) => {
        url = `<html><head><style>
    body{font-family:'sansserif';text-align:center;
    background-color:#141414;color:white}body>div{font-family:monospace;
    text-align:left;border:2px solid white;font-weight:600;
    width:98%;height:42%;padding:10px;overflow:auto;overflow-y:scroll}
    </style></head><body>Output (stdout)<div style='color:#58c3f7'>${out == null ? '' : out}
    </div>Errors (stderr)<div style='color: #ff1919'>${err == undefined ? '' : err}</div></body></html>`
        url = 'data:text/html;charset=utf-8,' + encodeURIComponent(url)
    }

    if (out === null || out.isDestroyed()) out = newWin()
    else out.focus()
    // out.loadURL(`file://${fileName}`)
    let tmpFile,
        tmpOptions = {
            mode: 0777,
            dir: tempDir,
            unsafeCleanup: true
        }
    switch (language) {
        case 'html':
            out.loadURL('data:text/html;charset=utf-8,' +
                encodeURIComponent(content))
            break
        case 'javascript':
            url = `<!DOCTYPE html>
<html><body><script type='text/javascript'>
${content}</script></body></html>`
            url = 'data:text/html;charset=utf-8,' +
                encodeURIComponent(url)
            out.loadURL(url)
            break
        case 'python':
            // tmpOptions.postfix = '.py'
            // tmpFile = tmp.fileSync(tmpOptions)
            // try {
            //     fs.writeFileSync(tmpFile.name, content)
            // } catch (err) {
            //     if (err) throw err
            // }
            // python.run(path.basename(tmpFile.name), {
            //     scriptPath: path.dirname(tmpFile.name)
            // }, (err, results) => {
            //     makeHTML(err, results)
            //     out.loadURL(url)
            // })
            // break
        case 'php':
            // break
        case 'ruby':
            tmpOptions.postfix = '.' + (language == 'ruby' ? 'rb' : language)
            tmpFile = tmp.fileSync(tmpOptions)
            try {
                fs.writeFileSync(tmpFile.name, content)
            } catch (err) {
                if (err) throw err
            }
            runner.exec(paths[language] + ` ${tmpFile.name}`, (err, response, stderr) => {
                makeHTML(stderr, response)
                out.loadURL(url)
                // if (err) throw err
            })
            break
        default:
            dialog.showErrorBox('Oops!', 'Invalid language selected!')
    }
    // NOTE: Do not add any code after the switch case block
    // It can break the application flow
}

exports.stopExec = () => {
    if (out !== null && !out.isDestroyed())
        out.stop()
}
